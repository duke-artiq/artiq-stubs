import typing

from .environment import NoDefault

__all__ = ["ScanObject",
           "NoScan", "RangeScan", "CenterScan", "ExplicitScan",
           "Scannable", "MultiScanManager"]


class ScanObject:
    def __iter__(self) -> typing.Iterator[typing.Any]:
        ...

    def __len__(self) -> int:
        ...

    def describe(self) -> typing.Dict[str, typing.Any]:
        ...


# Type variable for generics in scan classes
__V_T = typing.TypeVar('__V_T')


class NoScan(ScanObject, typing.Generic[__V_T]):
    def __init__(self, value: __V_T, repetitions: int = ...):
        ...

    def __iter__(self) -> typing.Iterator[__V_T]:
        ...

    def __len__(self) -> int:
        ...

    def describe(self) -> typing.Dict[str, typing.Any]:
        ...


class RangeScan(ScanObject):
    def __init__(self, start: float, stop: float, npoints: int,
                 randomize: bool = ..., seed: typing.Any = ...):
        ...

    def __iter__(self) -> typing.Iterator[float]:
        ...

    def __len__(self) -> int:
        ...

    def describe(self) -> typing.Dict[str, typing.Any]:
        ...


class CenterScan(ScanObject):
    def __init__(self, center: float, span: float, step: float,
                 randomize: bool = ..., seed: typing.Any = ...):
        ...

    def __iter__(self) -> typing.Iterator[float]:
        ...

    def __len__(self) -> int:
        ...

    def describe(self) -> typing.Dict[str, typing.Any]:
        ...


class ExplicitScan(ScanObject, typing.Generic[__V_T]):
    def __init__(self, sequence: typing.Sequence[__V_T]):
        ...

    def __iter__(self) -> typing.Iterator[__V_T]:
        ...

    def __len__(self) -> int:
        ...

    def describe(self) -> typing.Dict[str, typing.Any]:
        ...


class Scannable:

    unit: str
    scale: float
    global_step: float
    global_min: float | None
    global_max: float | None
    precision: int

    def __init__(self, default: typing.Union[typing.Type[NoDefault], ScanObject, typing.List[ScanObject]] = ...,
                 unit: str = ..., *, scale: typing.Optional[float] = ...,
                 global_step: typing.Optional[float] = ..., global_min: typing.Optional[float] = ...,
                 global_max: typing.Optional[float] = ..., precision: int = ...,
                 ndecimals: typing.Optional[int] = ...):
        ...

    def default(self) -> None:
        ...

    def process(self, x: typing.Dict[str, typing.Any]) -> ScanObject:
        ...

    def describe(self) -> typing.Dict[str, typing.Any]:
        ...


class MultiScanManager:
    def __init__(self, *args: typing.Tuple[str, typing.Any]):
        ...

    def __iter__(self) -> typing.Iterator[typing.Any]:
        ...
