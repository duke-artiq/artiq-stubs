import typing

from artiq.language.types import TInt64, TFloat, TNone

__all__ = [
    "kernel", "portable", "rpc", "syscall", "host_only",
    "kernel_from_string", "set_time_manager", "set_watchdog_factory",
    "TerminationRequested",
    "sequential", "parallel", "interleave",
    "delay_mu", "now_mu", "at_mu", "delay",
    "watchdog",
]

kernel_globals: typing.Tuple[str] = ...

__FN_T = typing.TypeVar('__FN_T', bound=typing.Callable[..., typing.Any])  # Type var for functions


@typing.overload
def kernel(arg: __FN_T, flags: typing.Set[str] = ...) -> __FN_T:
    ...


@typing.overload
def kernel(flags: typing.Set[str]) -> typing.Callable[[__FN_T], __FN_T]:
    ...


@typing.overload
def kernel(arg: typing.Optional[str] = ..., flags: typing.Set[str] = ...) -> typing.Callable[[__FN_T], __FN_T]:
    ...


@typing.overload
def subkernel(arg: __FN_T, destination: int = ..., flags: typing.Set[str] = ...) -> __FN_T:
    ...


@typing.overload
def subkernel(*, destination: int, flags: typing.Set[str]) -> typing.Callable[[__FN_T], __FN_T]:
    ...


@typing.overload
def subkernel(arg: typing.Optional[str] = ..., destination: int = ...,
              flags: typing.Set[str] = ...) -> typing.Callable[[__FN_T], __FN_T]:
    ...


@typing.overload
def portable(arg: __FN_T, flags: typing.Set[str] = ...) -> __FN_T:
    ...


@typing.overload
def portable(flags: typing.Set[str]) -> typing.Callable[[__FN_T], __FN_T]:
    ...


@typing.overload
def portable(arg: None = ..., flags: typing.Set[str] = ...) -> typing.Callable[[__FN_T], __FN_T]:
    ...


@typing.overload
def rpc(arg: __FN_T, flags: typing.Set[str] = ...) -> __FN_T:
    ...


@typing.overload
def rpc(flags: typing.Set[str]) -> typing.Callable[[__FN_T], __FN_T]:
    ...


@typing.overload
def rpc(arg: None = ..., flags: typing.Set[str] = ...) -> typing.Callable[[__FN_T], __FN_T]:
    ...


@typing.overload
def syscall(arg: __FN_T, flags: typing.Set[str] = ...) -> __FN_T:
    ...


@typing.overload
def syscall(flags: typing.Set[str]) -> typing.Callable[[__FN_T], __FN_T]:
    ...


@typing.overload
def syscall(arg: typing.Optional[str] = ..., flags: typing.Set[str] = ...) -> typing.Callable[[__FN_T], __FN_T]:
    ...


def host_only(function: __FN_T) -> __FN_T:
    ...


def kernel_from_string(parameters: typing.Sequence[typing.Union[str, typing.Tuple[str, str]]],
                       body_code: str,
                       decorator: typing.Callable[..., typing.Any] = ...) -> typing.Callable[..., typing.Any]:
    ...


def set_time_manager(time_manager: typing.Any) -> None:
    ...


# ARTIQ context managers
sequential: typing.ContextManager[None] = ...
parallel: typing.ContextManager[None] = ...
interleave = parallel


def delay_mu(duration: TInt64) -> TNone:
    ...


def now_mu() -> TInt64:
    ...


def at_mu(time: TInt64) -> TNone:
    ...


def delay(duration: TFloat) -> TNone:
    ...


def set_watchdog_factory(f: typing.Any) -> None:
    ...


def watchdog(timeout: TFloat) -> typing.ContextManager[None]:
    ...


class TerminationRequested(Exception):
    ...
