import typing
import numpy as np

__all__ = ["TNone", "TTuple",
           "TBool", "TInt32", "TInt64", "TFloat",
           "TStr", "TBytes", "TByteArray",
           "TList", "TArray", "TRange32", "TRange64",
           "TVar"]

TNone = type(None)
TBool = bool
TInt32 = typing.Union[int, np.int32]
TInt64 = typing.Union[int, np.int64]
TFloat = float
TStr = str
TBytes = bytes
TByteArray = bytearray
TRange32 = typing.Iterable[TInt32]
TRange64 = typing.Iterable[TInt64]
TVar = typing.Any  # Simplified typing

# The following types do not type-check well because they are functions
TList = lambda elt=...: typing.List  # noqa: E731
TArray = lambda elt=..., num_dims=...: np.ndarray  # noqa: E731
TTuple = lambda elts=...: typing.Tuple  # noqa: E731
