import typing
import types

import artiq.language.environment

__all__ = ["parse_arguments",
           "parse_devarg_override", "unparse_devarg_override",
           "elide", "scale_from_metadata",
           "short_format", "file_import",
           "get_experiment",
           "exc_to_warning", "asyncio_wait_or_cancel",
           "get_windows_drives", "get_user_config_dir"]


def parse_arguments(arguments: typing.Sequence[str]) -> typing.Dict[str, typing.Any]:
    ...


def parse_devarg_override(devarg_override: str) -> dict[str, typing.Any]:
    ...


def unparse_devarg_override(devarg_override: dict[str, typing.Any]) -> str:
    ...


def elide(s: str, maxlen: int) -> str:
    ...


def scale_from_metadata(metadata: dict[str, typing.Any]) -> float:
    ...


def short_format(v: typing.Any) -> str:
    ...


def file_import(filename: str, prefix: str = ...) -> types.ModuleType:
    ...


def get_experiment(module: types.ModuleType, class_name: typing.Optional[str] = ...) \
        -> typing.Type[artiq.language.Experiment]:
    ...


async def exc_to_warning(coro: typing.Coroutine[None, None, typing.Any]) -> None:
    ...


async def asyncio_wait_or_cancel(fs: typing.Sequence[typing.Any], **kwargs: typing.Any) -> typing.List[typing.Any]:
    ...


def get_windows_drives() -> typing.List[str]:
    ...


def get_user_config_dir() -> str:
    ...
