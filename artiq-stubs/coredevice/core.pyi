from __future__ import annotations  # Postponed evaluation of annotations

import typing

from artiq.language.core import portable, kernel
from artiq.language.types import TFloat, TInt32, TInt64, TBool, TNone

__all__ = ['CompileError', 'Core']


class CompileError(Exception):

    diagnostic: typing.Any

    def __init__(self, diagnostic: typing.Any):
        ...

    def __str__(self) -> str:
        ...


class Core:
    kernel_invariants: typing.ClassVar[set[str]] = ...

    ref_period: float
    ref_multiplier: int
    satellite_cpu_targets: dict[int, str]
    target_cls: type
    coarse_ref_period: float
    comm: typing.Any
    analyzer_proxy_name: typing.Optional[str]
    analyze_at_run_end: bool

    first_run: bool
    dmgr: typing.Any
    core: Core
    analyzer_proxy: typing.Any

    def __init__(self, dmgr: typing.Any, host: typing.Optional[str], ref_period: float,
                 analyzer_proxy: typing.Optional[str] = ..., analyze_at_run_end: bool = ...,
                 ref_multiplier: int = ..., target: str = ..., satellite_cpu_targets: dict[int, str] = ...) -> None:
        ...

    def notify_run_end(self) -> None:
        ...

    def close(self) -> None:
        ...

    def compile(
        self, function: typing.Any, args: typing.Sequence[typing.Any], kwargs: typing.Dict[str, typing.Any],
            set_result: typing.Any = ..., attribute_writeback: bool = ..., print_as_rpc: bool = ...,
            target: typing.Any = ..., destination: int = ..., subkernel_arg_types: list[typing.Any] = ...,
            old_embedding_map: typing.Any = ...
    ) -> tuple[typing.Any, typing.Any, typing.Any, typing.Any, typing.Any]:
        ...

    def _run_compiled(self, kernel_library: typing.Any, embedding_map: typing.Any, symbolizer: typing.Any,
                      demangler: typing.Any) -> None:
        ...

    def run(self, function: typing.Any,
            args: typing.Sequence[typing.Any], kwargs: typing.Dict[str, typing.Any]) -> typing.Any:
        ...

    def compile_subkernel(
        self, sid: typing.Any, subkernel_fn: typing.Any, embedding_map: typing.Any, args: typing.Sequence[typing.Any],
            subkernel_arg_types: typing.Any, subkernels: typing.Any
    ) -> tuple[typing.Any, typing.Any, typing.Any]:
        ...

    def compile_and_upload_subkernels(self, embedding_map: typing.Any, args: typing.Sequence[typing.Any],
                                      subkernel_arg_types: typing.Any) -> None:
        ...

    def precompile(self, function: typing.Any,
                   *args: typing.Any, **kwargs: typing.Any) -> typing.Callable[..., typing.Any]:
        ...

    @portable
    def seconds_to_mu(self, seconds: TFloat) -> TInt64:
        ...

    @portable
    def mu_to_seconds(self, mu: TInt64) -> TFloat:
        ...

    @kernel
    def get_rtio_counter_mu(self) -> TInt64:
        ...

    @kernel
    def wait_until_mu(self, cursor_mu: TInt64) -> TNone:
        ...

    @kernel
    def get_rtio_destination_status(self, destination: TInt32) -> TBool:
        ...

    @kernel
    def reset(self) -> TNone:
        ...

    @kernel
    def break_realtime(self) -> TNone:
        ...

    def trigger_analyzer_proxy(self) -> None:
        ...
