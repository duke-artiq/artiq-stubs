from enum import Enum
import typing

__all__ = [
    'Request', 'Reply',
    'UnsupportedDevice', 'LoadError', 'RPCReturnValueError',
    'RPCKeyword', 'receivers',
    'CommKernelDummy',
]


class Request(Enum):
    SystemInfo: typing.Final[int] = ...

    LoadKernel: typing.Final[int] = ...
    RunKernel: typing.Final[int] = ...

    RPCReply: typing.Final[int] = ...
    RPCException: typing.Final[int] = ...

    SubkernelUpload: typing.Final[int] = ...


class Reply(Enum):
    SystemInfo: typing.Final[int] = ...

    LoadCompleted: typing.Final[int] = ...
    LoadFailed: typing.Final[int] = ...

    KernelFinished: typing.Final[int] = ...
    KernelStartupFailed: typing.Final[int] = ...
    KernelException: typing.Final[int] = ...

    RPCRequest: typing.Final[int] = ...

    ClockFailure: typing.Final[int] = ...


class UnsupportedDevice(Exception):
    pass


class LoadError(Exception):
    pass


class RPCReturnValueError(ValueError):
    pass


RPCKeyword = typing.NamedTuple('RPCKeyword', [('name', str), ('value', typing.Any)])

receivers: dict[str, typing.Any] = ...


class CommKernelDummy:
    def __init__(self) -> None:
        ...

    def load(self, kernel_library: typing.Any) -> None:
        ...

    def run(self) -> None:
        ...

    def serve(self, embedding_map: typing.Any, symbolizer: typing.Any, demangler: typing.Any) -> None:
        ...

    def check_system_info(self) -> None:
        ...
