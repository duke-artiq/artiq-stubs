import typing

from artiq.language.core import kernel
from artiq.language.types import TInt32, TInt64, TFloat, TBool, TNone, TTuple

__all__ = [
    'CONFIG_COUNT_RISING', 'CONFIG_COUNT_FALLING', 'CONFIG_SEND_COUNT_EVENT', 'CONFIG_RESET_TO_ZERO',
    'CounterOverflow', 'EdgeCounter',
]


CONFIG_COUNT_RISING: int = ...
CONFIG_COUNT_FALLING: int = ...
CONFIG_SEND_COUNT_EVENT: int = ...
CONFIG_RESET_TO_ZERO: int = ...


class CounterOverflow(Exception):
    ...


class EdgeCounter:

    kernel_invariants: typing.ClassVar[set[str]]

    def __init__(self, dmgr: typing.Any, channel: int, gateware_width: int = ..., core_device: str = ...):
        ...

    @staticmethod
    def get_rtio_channels(channel: int, **kwargs: typing.Any) -> list[tuple[int, None]]:
        ...

    @kernel
    def gate_rising(self, duration: TFloat) -> TInt64:
        ...

    @kernel
    def gate_falling(self, duration: TFloat) -> TInt64:
        ...

    @kernel
    def gate_both(self, duration: TFloat) -> TInt64:
        ...

    @kernel
    def gate_rising_mu(self, duration_mu: TInt64) -> TInt64:
        ...

    @kernel
    def gate_falling_mu(self, duration_mu: TInt64) -> TInt64:
        ...

    @kernel
    def gate_both_mu(self, duration_mu: TInt64) -> TInt64:
        ...

    @kernel
    def _gate_mu(self, duration_mu: TInt64, count_rising: TBool, count_falling: TBool) -> TInt64:
        ...

    @kernel
    def set_config(self, count_rising: TBool, count_falling: TBool, send_count_event: TBool, reset_to_zero: TBool
                   ) -> TNone:
        ...

    @kernel
    def fetch_count(self) -> TInt32:
        ...

    @kernel
    def fetch_timestamped_count(self, timeout_mu: TInt64 = ...) -> TTuple([TInt64, TInt64]):  # type: ignore[valid-type]
        ...
