import typing
import numpy as np

import artiq.coredevice.core

from artiq.language.core import kernel, portable
from artiq.language.types import TInt32, TNone, TFloat


__all__ = [
    "SPI_DATA_ADDR", "SPI_CONFIG_ADDR",
    "SPI_OFFLINE", "SPI_END", "SPI_INPUT",
    "SPI_CS_POLARITY", "SPI_CLK_POLARITY", "SPI_CLK_PHASE",
    "SPI_LSB_FIRST", "SPI_HALF_DUPLEX",
    "SPIMaster", "NRTSPIMaster"
]

SPI_DATA_ADDR: int = ...
SPI_CONFIG_ADDR: int = ...

SPI_OFFLINE: int = ...
SPI_END: int = ...
SPI_INPUT: int = ...
SPI_CS_POLARITY: int = ...
SPI_CLK_POLARITY: int = ...
SPI_CLK_PHASE: int = ...
SPI_LSB_FIRST: int = ...
SPI_HALF_DUPLEX: int = ...


class SPIMaster:

    kernel_invariants: typing.ClassVar[set[str]] = ...

    core: artiq.coredevice.core.Core
    ref_period_mu: np.int64
    channel: int
    xfer_duration_mu: np.int64

    def __init__(self, dmgr: typing.Any, channel: int, div: int = ..., length: int = ..., core_device: str = ...):
        ...

    @staticmethod
    def get_rtio_channels(channel: int, **kwargs: typing.Any) -> list[tuple[int, None]]:
        ...

    @portable
    def frequency_to_div(self, f: TFloat) -> TInt32:
        ...

    @kernel
    def set_config(self, flags: TInt32, length: TInt32, freq: TFloat, cs: TInt32) -> TNone:
        ...

    @kernel
    def set_config_mu(self, flags: TInt32, length: TInt32, div: TInt32, cs: TInt32) -> TNone:
        ...

    @portable
    def update_xfer_duration_mu(self, div: TInt32, length: TInt32) -> TNone:
        ...

    @kernel
    def write(self, data: TInt32) -> TNone:
        ...

    @kernel
    def read(self) -> TInt32:
        ...


class NRTSPIMaster:

    core: artiq.coredevice.core.Core
    busno: int

    def __init__(self, dmgr: typing.Any, busno: int = ..., core_device: str = ...):
        ...

    @kernel
    def set_config_mu(self, flags: TInt32 = ..., length: TInt32 = ..., div: TInt32 = ..., cs: TInt32 = ...) -> TNone:
        ...

    @kernel
    def write(self, data: TInt32 = ...) -> TNone:
        ...

    @kernel
    def read(self) -> TInt32:
        ...
