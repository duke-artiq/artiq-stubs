import typing

import artiq.coredevice.core

import numpy

from artiq.language.core import kernel, portable
from artiq.language.types import TNone, TInt32, TInt64, TBool, TFloat


class TTLOut:

    kernel_invariants: typing.ClassVar[set[str]] = ...

    core: artiq.coredevice.core.Core
    channel: int
    target_o: int

    def __init__(self, dmgr: typing.Any, channel: int, core_device: str = ...) -> None:
        ...

    @staticmethod
    def get_rtio_channels(channel: int, **kwargs: typing.Any) -> list[tuple[int, None]]:
        ...

    @kernel
    def output(self) -> TNone:
        ...

    @kernel
    def set_o(self, o: TInt32) -> TNone:
        ...

    @kernel
    def on(self) -> TNone:
        ...

    @kernel
    def off(self) -> TNone:
        ...

    @kernel
    def pulse_mu(self, duration: TInt64) -> TNone:
        ...

    @kernel
    def pulse(self, duration: TFloat) -> TNone:
        ...


class TTLInOut:

    kernel_invariants: typing.ClassVar[set[str]] = ...

    core: artiq.coredevice.core.Core
    channel: int
    gate_latency_mu: numpy.int64
    target_o: int
    target_oe: int
    target_sens: int
    target_sample: int

    def __init__(self, dmgr: typing.Any, channel: int, gate_latency_mu: typing.Optional[int] = ...,
                 core_device: str = ...) -> None:
        ...

    @staticmethod
    def get_rtio_channels(channel: int, **kwargs: typing.Any) -> list[tuple[int, None]]:
        ...

    @kernel
    def set_oe(self, oe: TInt32) -> TNone:
        ...

    @kernel
    def output(self) -> TNone:
        ...

    @kernel
    def input(self) -> TNone:
        ...

    @kernel
    def set_o(self, o: TInt32) -> TNone:
        ...

    @kernel
    def on(self) -> TNone:
        ...

    @kernel
    def off(self) -> TNone:
        ...

    @kernel
    def pulse_mu(self, duration: TInt64) -> TNone:
        ...

    @kernel
    def pulse(self, duration: TFloat) -> TNone:
        ...

    @kernel
    def _set_sensitivity(self, value: TInt32) -> TNone:
        ...

    @kernel
    def gate_rising_mu(self, duration: TInt64) -> TInt64:
        ...

    @kernel
    def gate_falling_mu(self, duration: TInt64) -> TInt64:
        ...

    @kernel
    def gate_both_mu(self, duration: TInt64) -> TInt64:
        ...

    @kernel
    def gate_rising(self, duration: TFloat) -> TInt64:
        ...

    @kernel
    def gate_falling(self, duration: TFloat) -> TInt64:
        ...

    @kernel
    def gate_both(self, duration: TFloat) -> TInt64:
        ...

    @kernel
    def count(self, up_to_timestamp_mu: TInt64) -> TInt32:
        ...

    @kernel
    def timestamp_mu(self, up_to_timestamp_mu: TInt64) -> TInt64:
        ...

    @kernel
    def sample_input(self) -> TNone:
        ...

    @kernel
    def sample_get(self) -> TInt32:
        ...

    @kernel
    def sample_get_nonrt(self) -> TInt32:
        ...

    @kernel
    def watch_stay_on(self) -> TBool:
        ...

    @kernel
    def watch_stay_off(self) -> TBool:
        ...

    @kernel
    def watch_done(self) -> TBool:
        ...


class TTLClockGen:

    kernel_invariants: typing.ClassVar[set[str]] = ...

    core: artiq.coredevice.core.Core
    channel: int
    target: int
    acc_width: numpy.int64

    def __init__(self, dmgr: typing.Any, channel: int, acc_width: int = ..., core_device: str = ...):
        ...

    @staticmethod
    def get_rtio_channels(channel: int, **kwargs: typing.Any) -> list[tuple[int, None]]:
        ...

    @portable
    def frequency_to_ftw(self, frequency: TFloat) -> TInt32:
        ...

    @portable
    def ftw_to_frequency(self, ftw: TInt32) -> TFloat:
        ...

    @kernel
    def set_mu(self, frequency: TInt32) -> TNone:
        ...

    @kernel
    def set(self, frequency: TFloat) -> TNone:
        ...

    @kernel
    def stop(self) -> TNone:
        ...
