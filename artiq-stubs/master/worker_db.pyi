import typing
import logging

__all__ = ['logger', 'DummyDevice', 'DeviceError', 'DeviceManager', 'DatasetManager']

logger: logging.Logger = ...


class DummyDevice:
    ...


class DeviceError(Exception):
    ...


class DeviceManager:

    ddb: typing.Any
    virtual_devices: dict[str, typing.Any]
    active_devices: list[typing.Any]
    devarg_override: dict[str, typing.Any]

    def __init__(self, ddb: typing.Any, virtual_devices: typing.Dict[str, typing.Any] = ...):
        ...

    def get_device_db(self) -> dict[str, typing.Any]:
        ...

    def get_desc(self, name: str) -> typing.Any:
        ...

    def get(self, name: str) -> typing.Any:
        ...

    def notify_run_end(self) -> None:
        ...

    def close_devices(self) -> None:
        ...


class DatasetManager:

    local: dict[str, typing.Any]
    archive: dict[str, typing.Any]
    metadata: dict[str, typing.Any]

    ddb: typing.Any

    def __init__(self, ddb: typing.Any):
        ...

    def set(self, key: str, value: typing.Any, broadcast: bool = ..., persist: bool = ..., archive: bool = ...) -> None:
        ...

    def mutate(self, key: str, index: typing.Any, value: typing.Any) -> None:
        ...

    def append_to(self, key: str, value: typing.Any) -> None:
        ...

    def get(self, key: str, archive: bool = ...) -> typing.Any:
        ...

    def get_metadata(self, key: str) -> dict[str, typing.Any]:
        ...

    def write_hdf5(self, f: typing.Any) -> None:
        ...
