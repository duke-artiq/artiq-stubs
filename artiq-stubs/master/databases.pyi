import typing

__all__ = ['device_db_from_file', 'DeviceDB', 'DatasetDB', 'InteractiveArgDB']


def device_db_from_file(filename: str) -> typing.Dict[str, typing.Any]:
    ...


class DeviceDB:

    backing_file: str
    data: typing.Any

    def __init__(self, backing_file: str):
        ...

    def scan(self) -> None:
        ...

    def get_device_db(self) -> typing.Dict[str, typing.Any]:
        ...

    def get(self, key: str, resolve_alias: bool = ...) -> typing.Any:
        ...

    def get_satellite_cpu_target(self, destination: int) -> typing.Any:
        ...


class DatasetDB:

    persist_file: str
    autosave_period: int
    lmdb: typing.Any
    data: typing.Any
    pending_keys: set[str]

    def __init__(self, persist_file: str, autosave_period: int = ...):
        ...

    def close_db(self) -> None:
        ...

    def save(self) -> None:
        ...

    def get(self, key: str) -> typing.Any:
        ...

    def get_metadata(self, key: str) -> typing.Any:
        ...

    def update(self, mod: typing.Any) -> None:
        ...

    def set(self, key: str, value: typing.Any, persist: typing.Optional[bool] = ...,
            metadata: dict[str, typing.Any] | None = ...) -> None:
        ...

    def delete(self, key: str) -> None:
        ...


class InteractiveArgDB:

    pending: typing.Any
    futures: dict[int, typing.Any]

    def __init__(self) -> None:
        ...

    async def get(self, rid: int, arglist_desc: typing.Any, title: str) -> typing.Any:
        ...

    def supply(self, rid: int, values: typing.Any) -> None:
        ...

    def cancel(self, rid: int) -> None:
        ...
