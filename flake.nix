{
  inputs = {
    artiqpkgs.url = git+https://github.com/m-labs/artiq?ref=release-8;
    nixpkgs.follows = "artiqpkgs/nixpkgs";
    flake8-artiq = {
      url = git+https://gitlab.com/duke-artiq/flake8-artiq.git;
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, artiqpkgs, flake8-artiq }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;

      artiq-stubs = pkgs.python3Packages.callPackage ./derivation.nix {
        inherit (artiqpkgs.packages.x86_64-linux) artiq;
      };

    in
    {
      packages.x86_64-linux = {
        inherit artiq-stubs;
        default = artiq-stubs;
      };

      devShells.x86_64-linux = {
        default = pkgs.mkShell {
          packages = [
            (pkgs.python3.withPackages (ps: [
              ps.numpy
              ps.mypy
              flake8-artiq.packages.x86_64-linux.flake8-artiq
            ] ++ artiq-stubs.propagatedBuildInputs))
          ];
        };
      };

      # enables use of `nix fmt`
      formatter.x86_64-linux = pkgs.nixpkgs-fmt;
    };

  nixConfig = {
    extra-trusted-public-keys = [
      "nixbld.m-labs.hk-1:5aSRVA5b320xbNvu30tqxVPXpld73bhtOeH6uAjRyHc="
    ];
    extra-substituters = [ "https://nixbld.m-labs.hk" ];
  };
}
