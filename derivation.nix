{ buildPythonPackage
, nix-gitignore
, lib
, setuptools
, numpy
, artiq
}:

buildPythonPackage {
  pname = "artiq-stubs";
  version = "8";
  src = nix-gitignore.gitignoreSource [ "*.nix" "/*.lock" ] ./.;
  format = "pyproject";

  doCheck = false;
  dontWrapQtApps = true;

  nativeBuildInputs = [
    setuptools
  ];

  propagatedBuildInputs = [
    numpy
    artiq
  ];

  condaDependencies = [
    "numpy<2"
    "artiq==8.*"
  ];

  meta = with lib; {
    description = "ARTIQ stubs";
    maintainers = [ "Duke University" ];
    homepage = "https://gitlab.com/duke-artiq/artiq-stubs";
    license = licenses.lgpl3Only;
  };
}
